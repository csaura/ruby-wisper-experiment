require 'wisper'
require 'wisper/celluloid'

class FollowersDeletedHandler
  def self.followers_deleted(attributes)
    puts 'Followers deleted'
    raise ArgumentError
    puts attributes
  end
end

class EmploymentsDeletedHandler
  def self.employments_deleted(attributes)
    puts 'Employments deleted'
    puts attributes
  end

  def self.followers_deleted(attributes)
    puts 'Followers inside employments deleted'
    puts attributes
  end
end


Wisper.subscribe(FollowersDeletedHandler, async: true)
Wisper.subscribe(EmploymentsDeletedHandler)

module DataPrivacy
  class DeleteUser
    extend Wisper::Publisher

    def self.call(user_id)
      puts 'DataDeletion::User.delete_followers'
      broadcast(:followers_deleted, {user_id: user_id})
      puts 'DataDeletion::User.delete_followers'
      broadcast(:employments_deleted, {user_id: user_id})
    end
  end
end

DataPrivacy::DeleteUser.call(2)
