require 'wisper'
require 'wisper/celluloid'


$hash_storage = {"2" => [], "3" => []}

module DataPrivacy
  class User
    include Wisper::Publisher

    def initialize(user_id)
      @user_id = user_id
    end

    def delete_followers
      puts "user_followers_deleted # #{user_id}"
      $hash_storage[user_id.to_s] << 'user_followers_deleted'
      broadcast(:user_followers_deleted, { user_id: user_id} )
    end

    def delete_visitors
      puts "user_visitors_deleted # #{user_id}"
      $hash_storage[user_id.to_s] << 'user_visitors_deleted'
      broadcast(:user_visitors_deleted, { user_id: user_id} )
    end

    def delete_employments
      puts "user_employments_deleted # #{user_id}"
      $hash_storage[user_id.to_s] << 'user_employments_deleted'
      broadcast(:user_employments_deleted, { user_id: user_id} )
    end

    private
    attr_reader :user_id
  end
end


module DataPrivacy
  class UserDeletionProgress
    def initialize(user_id)
      @user_id = user_id
    end

    def completed?
      return false if already_completed?
      completness =  %w(user_visitors_deleted user_followers_deleted user_employments_deleted)  - deletions
      completness.empty?
    end

    private
    attr_reader :user_id

    def already_completed?
      deletions.include?('all_user_data_deleted')
    end

    def deletions
      $hash_storage[user_id.to_s]
    end
  end
end

module DataPrivacy
  class UserDeletionProcessManager #SAGA
    extend Wisper::Publisher

    def self.user_followers_deleted(user_id:)
      check_completeness(user_id)
    end

    def self.user_employments_deleted(user_id:)
      check_completeness(user_id)
    end

    def self.user_visitors_deleted(user_id:)
      check_completeness(user_id)
    end

    def self.check_completeness(user_id)
      progress = UserDeletionProgress.new(user_id)

      if progress.completed?
        puts "all_user_data_deleted # user_id #{user_id}"
        $hash_storage[user_id.to_s] << 'all_user_data_deleted'
        broadcast(:all_user_data_deleted, { user_id: user_id} )
      end
    end
    private_class_method :check_completeness
  end
end

module DataPrivacy
  class UserDeletedProcessor
    extend Wisper::Publisher

    def self.process(attributes)
      user_id = attributes[:user_id]
      puts "Start Data deletion # user_id #{user_id}"
      broadcast(:user_data_requested, {user_id: user_id})
      puts "Data deletion broadcasted # user_id #{user_id}"
    end
  end
end

module DataPrivacy
  class DeleteFollowers
    def self.user_data_requested(user_id:)
      puts "DataPrivacy::User.delete_followers # user_id #{user_id}"
      User.new(user_id).delete_followers
    end
  end
end

module DataPrivacy
  class DeleteEmployments
    def self.user_data_requested(user_id:)
      puts "DataPrivacy::User.delete_employments # user_id #{user_id}"
      User.new(user_id).delete_employments
    end
  end
end

module DataPrivacy
  class DeleteVisitors
    def self.user_data_requested(user_id:)
      puts "DataPrivacy::User.delete_visitors # user_id #{user_id}"
      User.new(user_id).delete_visitors
    end
  end
end

Wisper.subscribe(DataPrivacy::DeleteVisitors, async: true)
Wisper.subscribe(DataPrivacy::DeleteFollowers, async: true)
Wisper.subscribe(DataPrivacy::DeleteEmployments, async: true)
Wisper.subscribe(DataPrivacy::UserDeletionProcessManager, async: true)

DataPrivacy::UserDeletedProcessor.process({ user_id: 2 })
DataPrivacy::UserDeletedProcessor.process({ user_id: 3 })
