# Wisper execise

Small ruby script to play with wisper and celluloid.

It uses some concepts of DDD

> bundle exec ruby app.rb

> bundle exec ruby data_deletion.rb


Some resources for the future:
https://blog.carbonfive.com/2017/07/18/evented-rails-decoupling-complex-domains-in-rails-with-domain-events/

https://www.toptal.com/ruby-on-rails/the-publish-subscribe-pattern-on-rails


https://www.sitepoint.com/using-wisper-to-decompose-applications/
